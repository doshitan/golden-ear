with (import <nixpkgs> {}).pkgs;
callPackage ./. { inherit (elmPackages) elm-compiler elm-make elm-package elm-reactor elm-repl; }
