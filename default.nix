{ elm-compiler, elm-make, elm-package, elm-reactor
, elm-repl, stdenv
}:
stdenv.mkDerivation rec {
  name = "game-jam-${version}";
  version = "0.1.0.0";
  src = ./.;
  buildInputs = [
    elm-compiler elm-make elm-package elm-reactor elm-repl
  ];
  meta = {
    license = stdenv.lib.licenses.agpl3;
  };
}
