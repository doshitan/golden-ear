# Golden Ear

*Tactical Listening Action*

![Golden Ear Logo](assets/imgs/logo_blue.png)

You take on the role of a spy with slightly better than normal human hearing.
We need to listen in on a specific conversation, but the involved parties are
using advanced electronic jamming, rendering normal surveillance equipment
useless. Your mission, should you choose to accept it, is to focus in on the
conversation while ignoring distractions. This is represented by keeping a
moving ring between two other rings. While your focus is in this zone, you can
hear the conversation, otherwise it is lost to the background. Complete the
mission and report back for debriefing.

Developed in a weekend for the ICT Game Jam 2015 whose theme was useless
powers. It won the Judge's Choice.

Note, it currently only runs well in Chrome/Chromium (other WebKit/Blink
browsers are probably ok as well) and Firefox >= 42. Older Firefox releases
have some stuttering issues. Not tested in IE.

Give it a go at: https://doshitan.gitlab.io/golden-ear/
